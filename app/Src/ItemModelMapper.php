<?php


namespace App\Src;


use App\Model\Item;

class ItemModelMapper
{
    /**
     * @param $request
     * @param $merchant_id
     * @return Item
     */
    public static function toEloquentModel($request, $merchant_id)
    {
        $item = new Item();
        $item->title = $request['name'];
        $item->price = $request['price']['amount'];
        $item->merchant_id = $merchant_id;
//
        return $item;
    }
}
