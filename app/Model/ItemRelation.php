<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemRelation extends Model
{
    protected $fillable = [
        'item_id',
        'related_item_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * belongs to 1 Item
     */
    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function relatedItems()
    {
        return $this->belongsToMany(Item::class,'item_related')->withPivot(['related_item_id']);
    }
}
