<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Merchant extends Model
{
    protected $fillable = [
        'name',
        'description',
        'logo',
        'website',
        'feed_xml',
        'active',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * one to many relation to Item
     */
    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function makeActive()
    {
        $this->update(['active' => 1]);
    }

    public function makeInActive()
    {
        $this->update(['active' => 0]);
    }

    public function checkIfUserActive()
    {
        if ($this->active === '1') {
            return 'Active';
        } else {
            return 'Inactive';
        }
    }
}
