<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Item extends Model
{
//    protected $fillable = [
//        'title',
//        'description',
//        'price',
//        'merchant_id'
//    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * inverse relation to Merchant
     */
    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'item_attributes')->withPivot(['default_value']);
    }

    public function keywords()
    {
        return $this->belongsToMany(Keyword::class, 'item_keywords')->withPivot(['name']);
    }

    public function images()
    {
        return $this->hasMany(ItemImage::class,'item_id');
    }

    public function searchableAs()
    {
        return 'posts_index';
    }


}
