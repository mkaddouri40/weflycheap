<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $fillable = [
        'name',
        'translation',
        'default_value'
    ];
    
    Public function getTranslationAttribute()
    {
        Return __($this->attributes['translation']);
    }
}
