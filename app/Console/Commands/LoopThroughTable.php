<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Redis;
use App\Jobs\DownloadFeed;
use App\Jobs\ProcessFeed;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use App\Model\Merchant;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Log;

class LoopThroughTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'find:merchants';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Loops through a table of merchants';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DownloadFeed::dispatch();
        //downloadfeed
//        $job1 = (new ProcessFeed(Carbon::today()))->onQueue('default');
//        dispatch($job1);
    }
}
