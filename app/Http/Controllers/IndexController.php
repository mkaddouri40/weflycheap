<?php

namespace App\Http\Controllers;

use App\Model\Item;
use App\Model\ItemImage;
use App\Model\Merchant;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $q = $request->get('keyword');
        $q = strtolower($q);
        $orginalKeyword = $request->get('originalKeyword', $q);
        $previoussortby = $request->get('previoussortby');
        $sortby = $request->get('sortby', 'sorteer op');
        $minPrice = $request->get('min_price', 0);
        $maxMaxPrice = ceil(Item::max('price'));
        $maxPrice = $request->get('max_price', $maxMaxPrice);

        $merchants = Merchant::all();

        /**use this if you want to retrieve evertything
         * $query = Item::query();\
         */
        $query = Item::query();
        $q = $request->get('keyword');
        //alleen zoeken op keyword
        if ($request->keyword) {

            $query = $query
                ->where(function ($query) use ($q) {
                    $query->where('title', 'LIKE', '%' . $q . '%');
                    $query->orWhere('description', 'LIKE', '%' . $q . '%');
                    $query->orWhere('brand', '=', $q);
                });
        }

        if (!empty($request->min_price) || !empty($request->max_price)) {
            $query->whereBetween('price', [$minPrice, $maxPrice]);
        }
        //sorteren op prijs oplopend
        if ($request->get('sortby') == "prijs oplopend") {
            $query->orderBy('price', 'asc');
        }

        if ($request->get('sortby') == "prijs aflopend") {
            $query->orderBy('price', 'desc');
        }
        if ($request->get('sortby') == "merk oplopend") {
            $query->orderBy('brand', 'asc');
        }
        if ($request->get('sortby') == "merk aflopend") {
            $query->orderBy('brand', 'desc');
        }

        $results = $query->paginate(20);

        return view('index', [
            'items' => $results->appends(Input::except('page')),
            'maxPrice' => $maxPrice,
            'maxMaxPrice' => $maxMaxPrice,
            'minPrice' => $minPrice,
            'merchants' => $merchants,
            'keyword' => $q,
            'originalKeyword' => $orginalKeyword,
            'sortby' => $sortby,
            'previoussortby' => $previoussortby
        ]);
    }

    //return a view for a single item
    public function show(Request $request, $slug_title)
    {
        $item = Item::where('slug_title', $slug_title)->firstOrFail();

        $keyword = $request->get('keyword', '');

        $items = Item::where(function ($items) use ($item, $slug_title) {
            $items->where('id', '=', $item->id);
        });
        $images = ItemImage::where('item_id', $item->id)->get();


        return view('pages.items.show', compact('item', 'images', 'items', 'keyword', 'slug_title'));
    }

    public function duplicate(Merchant $merchant, Item $item, Request $request)
    {
        $images = ItemImage::where('item_id', $item->id)->get();
        $items = Item::where('title', '=', $item->title)
            ->orWhere('description', '=', $item->description)
            ->orWhere('sku', '=', $item->sku)->get();
        return view('pages.items.duplicate', compact('item', 'images', 'items', 'merchant'));
    }

    //Merken pagina
    public function brands(Request $request)
    {
        $q = $request->get('keyword');
        $orginalKeyword = $request->get('originalKeyword', $q);
        $previoussortby = $request->get('previoussortby');
        $sortby = $request->get('sortby', 'sorteer op');
        $minPrice = $request->get('min_price', 0);
        $maxMaxPrice = ceil(Item::max('price'));
        $maxPrice = $request->get('max_price', $maxMaxPrice);
        $ascordesc = $request->get('ascordesc', 'sorteer op');
        $ascordesc = $ascordesc == 'merk aflopend' ? ' desc' : 'asc';
        $letter = $request->get('brand_letter', '');
        $q = $request->get('keyword');


        $merchants = Merchant::all();
        if ($letter == '*') {
            $query = Item::query()->select('brand')->distinct()->orderBy('brand', 'asc');
        } else {
            if (!empty($request->keyword) && $letter == '*') {
                $query = Item::query();
                $query->where('brand', 'LIKE', '%' . $q . '%');
            }
            if ($letter == '') {
                $query = Item::query()->select('brand')->distinct()->orderBy('brand', $ascordesc);
            } else {
                $query = Item::where('brand', 'like', $letter . '%')->select('brand')->distinct()->orderBy('brand', $ascordesc);

            }

        }
        $results = $query->get();

        $sorteringmerk = $request->get('ascordesc');
        return view('pages.brands.index',
            ['items' => $results,
                'merchants' => $merchants,
                'letter' => $letter,
                'keyword' => $q,
                'sorteringmerk' => $sorteringmerk,
                'maxPrice' => $maxPrice,
                'maxMaxPrice' => $maxMaxPrice,
                'minPrice' => $minPrice,
                'originalKeyword' => $orginalKeyword,
                'sortby' => $sortby,
                'previoussortby' => $previoussortby

            ]);
    }

    //Producten pagina per merk
    public function productByBrand(Request $request, $brand, Merchant $merchant)
    {
        $q = $request->get('keyword');
        $orginalKeyword = $request->get('originalKeyword', $q);
        $previoussortby = $request->get('previoussortby');
        $sortby = $request->get('sortby', 'sorteer op');
        $minPrice = $request->get('min_price', 0);
//        $maxMaxPrice = ceil(Item::max('price'));
        $maxMaxPrice = ceil(Item::where('brand', $brand)->max('price'));
        $maxPrice = $request->get('max_price', $maxMaxPrice);


        $query = Item::query()->where('brand', $brand);
        if ($request->keyword) {
            $query
                ->where(function ($query) use ($q) {
                    $query->where('title', 'LIKE', '%' . $q . '%');
                    $query->orWhere('description', 'LIKE', '%' . $q . '%');
                });
        }
        if (!empty($request->min_price) || !empty($request->max_price)) {
            $min = $request->get('min_price');
            $max = $request->get('max_price');
            $query = $query->whereBetween('price', [$min, $max]);
        }

        if ($request->get('sortby') == "prijs oplopend") {
            $query->orderBy('price', 'asc');
        }

        if ($request->get('sortby') == "prijs aflopend") {
            $query->orderBy('price', 'desc');
        }

        $merchants = Merchant::all();
        $results = $query->paginate(20);
        return view('pages.brands.brand',
            ['items' => $results->appends(Input::except('page')),
                'merchants' => $merchants,
                'brand' => $brand,
                'keyword' => $q,
                'maxPrice' => $maxPrice,
                'maxMaxPrice' => $maxMaxPrice,
                'minPrice' => $minPrice,
                'merchant' => $merchant,
                'originalKeyword' => $orginalKeyword,
                'sortby' => $sortby,
                'previoussortby' => $previoussortby
            ]);
    }

    public function merchantsView(Request $request)
    {
        $q = $request->keyword;
        $minPrice = 0;
        $maxPrice = ceil(Item::max('price'));
        $maxMaxPrice = $maxPrice;

        if ($request->get('keyword') != $request->get('originalKeyword')) {
            $request->get('min_price', 0);
            $q = $request->get('keyword');
        } else {
            $q = $request->get('keyword');
            $q = $request->get('keyword');
            $minPrice = $request->get('min_price', 0);
            $maxPrice = $request->get('max_price', $maxPrice);
        }
        $merchants = Merchant::all();
        /**use this if you want to retrieve evertything
         * $query = Item::query();\
         */
        $query = Merchant::query();
        if ($request->keyword) {
            $query = $query->where('name', 'LIKE', '%' . $q . '%')
                ->orWhere('description', 'LIKE', '%' . $q . '%');
        }
        $results = $query->paginate(20);
        $q = $request->get('keyword', '');
        return view('pages.merchants.index',
            ['merchants' => $results,
                'keyword' => $q,
                'maxPrice' => $maxPrice,
                'maxMaxPrice' => $maxMaxPrice,
                'minPrice' => $minPrice
            ]);
    }

    public function itemsByMerchant(Request $request, $merchant)
    {
        dump($merchant);
        $merchant = str_replace('-', ' ', $merchant);
        $q = $request->get('keyword');
        $q = strtolower($q);
        $orginalKeyword = $request->get('originalKeyword', $q);
        $previoussortby = $request->get('previoussortby');
        $sortby = $request->get('sortby', 'sorteer op');
        $minPrice = $request->get('min_price', 0);
        $maxMaxPrice = ceil(Item::max('price'));
        $maxPrice = $request->get('max_price', $maxMaxPrice);
        $id = Merchant::where('name', $merchant)->pluck('id');
        $sortby = $request->get('sortby', 'sorteer op');
        $minPrice = $request->get('min_price', 0);
        $maxPrice = $request->get('max_price', $maxPrice);

        $merchants = Merchant::all();
        $query = Item::where('merchant_id', $id);

        //alleen zoeken op keyword
        if ($request->keyword) {
            $query = $query
                ->where(function ($query) use ($q) {
                    $query->where('title', 'LIKE', '%' . $q . '%');
                    $query->orWhere('description', 'LIKE', '%' . $q . '%');
                });
        }

        //alleen filteren op prijs
        if (!empty($request->min_price) || !empty($request->max_price)) {
            $min = $request->get('min_price');
            $max = $request->get('max_price');
            $query->whereBetween('price', [$min, $max]);

        }

        //sorteren op prijs oplopend
        if ($sortby == "prijs oplopend") {
            $query->orderBy('price', 'asc');
        }

        if ($sortby == "prijs aflopend") {
            echo 'prijs aflopend';
            $query->orderBy('price', 'desc');
        }

        if ($sortby == "merk oplopend") {
            echo 'merk oplopend';
            $query->orderBy('brand', 'asc');
        }

        if ($sortby == "merk aflopend") {
            echo 'merk oplopend';
            $query->orderBy('brand', 'desc');
        }

        $results = $query->paginate(20);
        return view('pages.merchants.items',
            ['items' => $results->appends(Input::except('page')),
                'maxPrice' => $maxPrice,
                'maxMaxPrice' => $maxMaxPrice,
                'minPrice' => $minPrice,
                'merchants' => $merchants,
                'merchant' => $merchant,
                'keyword' => $q,
                'originalKeyword' => $orginalKeyword,
                'sortby' => $sortby,
                'previoussortby' => $previoussortby
            ]);
    }

    public function about(Request $request)
    {
        $q = $request->get('keyword', '');
        return view('about', ['keyword' => $q]);

    }

    public function privacy(Request $request)
    {
        $q = $request->get('keyword', '');
        return view('pages/privacy', ['keyword' => $q]);

    }

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
