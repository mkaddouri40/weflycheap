<?php

namespace App\Http\Controllers;

use App\Http\Requests\MerchantSaveRequest;
use App\Model\Merchant;
use Illuminate\Http\Request;

class MerchantsController extends Controller
{
    // shows a list of merchants GET
    public function index()
    {
        $merchants = Merchant::all();
        return view('merchants.all', compact('merchants'));
    }

    // shows a form to create a merchant GET
    public function create(Merchant $merchant)
    {
        return view('merchants.create', compact('merchant'));
    }

    // stores the data we received from the create form POST
    // or updates a merchant with the new data we received from the edit form POST
    public function store(MerchantSaveRequest $request, Merchant $merchant)
    {
        if ($merchant->exists) {
            $merchant->update($request->all());
            \Session::flash('message', 'Merchant update');
            \Session::flash('alert-class', 'alert alert-success');
        } else {
            $merchant = Merchant::create($request->all());
            \Session::flash('message', 'New Merchant Created');
            \Session::flash('alert-class', 'alert alert-success');
            //WORDT GEBRUIKT ALS TEST VOOR MEZELF
        }


        return redirect()->route('weflycheap.merchants.index');
    }

    // returns a form pre-filled with a merchants data... so that we can edit this merchant GET
    public function edit(MerchantSaveRequest $request, Merchant $merchant)
    {
        return view('merchants.edit', compact('merchant'));
    }

    //delete the current merchant
    public function destroy(MerchantSaveRequest $request, Merchant $merchant)
    {
        $merchant->delete();
        return response()->json(['success' => 'RECORD HAS BEEN DELETED']);
    }
}
