<?php
//ini_set('max_execution_time', 180);

namespace App\Jobs;

use App\ItemItemImages;
use App\Src\ItemModelMapper;
use Illuminate\Support\Facades\Redis;
use App\Model\Item;
use App\Model\ItemImage;
use App\Model\Merchant;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

//use App\Model\Merchant;
//use App\Model\Item;
class ProcessFeed implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $url;
    public $xml_id;

    public function __construct($url, $xml_id)
    {
        $this->url = $url;
        $this->xml_id = $xml_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $merchant = Merchant::findOrFail($this->xml_id);
        //heeft merchant prodcuten ?
        $xml_id = $this->xml_id;
        $feed = \Storage::get($xml_id);
        $feed = json_decode($feed, true);
        $products = $feed['products'];
        foreach ($products as $product) {
            $merchant = Merchant::findOrFail($this->xml_id);
            $xml_id = $this->xml_id;
            $feed = \Storage::get($xml_id);
            $feed = json_decode($feed, true);
            $products = $feed['products'];
            $item = new Item();
            $item->title = $product['name'];
            $item->price = $product['price']['amount'];
            $item->slug_title = self::slugify($product['name']);
            $item->description = $product['description'];
            $item->merchant_id = $merchant->id;
            $item->save();

            $itemImage = new ItemImage();
            $itemImage->item_id = $item->id;
            $itemImage->location = $product['images'][0];
            $itemImage->position = 'updated';
            $itemImage->save();
        }
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
