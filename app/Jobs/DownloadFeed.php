<?php

namespace App\Jobs;

use App\Http\Requests\MerchantSaveRequest;
use App\Model\Merchant;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DownloadFeed implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $merchants = Merchant::all();
        $i = 0;
        foreach ($merchants as $merchant) {
            $xml_feed = $merchant->feed_xml;
            $xml_id = $merchant->id;

            //make a requst with guzzle and
            $client = new Client(['base_uri' => $xml_feed]);
            $res = $client->request('GET', $xml_feed);
//            dump($res->getBody()->getContents());
            $response = $res->getBody()->getContents();
            //fetch body content...
            $xml_downloaded_feed = $response;

            // end of guzzle process
            $url = \Storage::url($xml_id);
            if (!\Storage::disk('local')->has($xml_id)) {
                \Storage::disk('local')->put($xml_id, $xml_downloaded_feed);
                ProcessFeed::dispatch($url, $xml_id);
                echo 'bestand opgeslagen hier';
            } elseif (\Storage::disk('local')->has($xml_id) && strcmp($xml_downloaded_feed, \Storage::get($xml_id)) != 0) {
                echo 'Bewaar bestaat maar niet gelijk';
                ProcessFeed::dispatch($url, $xml_id);
                \Storage::disk('local')->put($xml_id, $xml_downloaded_feed);
            }
        }
    }
}
//strcmp($xml_downloaded_feed, \Storage::get($xml_id)) == 0

