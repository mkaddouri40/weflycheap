<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

//index
Breadcrumbs::register('home', function ($trail) {
    $trail->push('Home',route('index'));
});

//index -> item page
Breadcrumbs::register('item',function ($trail, \App\Model\Item $item) {
    $trail->parent('home');
    $trail->push($item->title, url('item/{slug_title}'));
});

// index -> brands
Breadcrumbs::register('brands',function ($trail) {
    $trail->parent('home');
    $trail->push('merken', url('brands'));
});
// index -> brands -> brand-products
Breadcrumbs::register('brand',function ($trail, $brand) {
    $trail->parent('brands');
    $trail->push($brand, url('brands/{brand}'));
});

// index -> brands
Breadcrumbs::register('merchants',function ($trail) {
    $trail->parent('home');
    $trail->push('Aanbieders', url('merchants'));
});
// index -> brands -> brand-products
Breadcrumbs::register('items',function ($trail, $merchant) {
    $trail->parent('merchants');
    $trail->push($merchant, url('merchants/{merchant}'));
});