<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Model\Item;
use App\Model\Merchant;

Route::get('/test', function () {
    $feed = \Storage::get('1');
    $feed = json_decode($feed, true);
    $products = $feed['products'];
    foreach ($products as $product) {
        dump($product['images'][0]);
//        foreach ($product['price'] as $price) {
//            $price = json_decode($price,true);
//            dump($price['amount']);
//        }
    }
});
//Route::get('/', 'IndexController@home')->name('home');
Route::get('/', 'IndexController@index')->name('index');
//Route::get('/item/{merchant}/{item}/{slug_title}', 'IndexController@show')->name('show');
Route::get('/item/{slug_title}', 'IndexController@show')->name('show');
Route::post('/', 'IndexController@mySearch');

Route::get('/about', 'IndexController@about');


Route::get('/privacy-policy', 'IndexController@privacy');


//toont alle merken`
Route::get('/brands', 'IndexController@brands')->name('brands');
Route::get('/brand/{brand}', 'IndexController@productByBrand')->name('brand');

//toon alle merchants
Route::get('/merchants', 'IndexController@merchantsView')->name('merchants');
Route::get('/merchant/{merchant}', 'IndexController@itemsByMerchant')->name('items');

Route::group(['prefix' => '/weflycheap', 'as' => 'weflycheap.'], function () {
    Route::group(['prefix' => '/merchants', 'as' => 'merchants.'], function () {
        //fetches all merchants
        Route::get('/', 'MerchantsController@index')->name('index');
        //stores new merchant
        Route::post('/{merchant?}', 'MerchantsController@store')->name('store');
        //creates the form/view to create a merchant
        Route::get('/create', 'MerchantsController@create')->name('create');
        //Renders a form then fetches the current merchant
        Route::get('/edit/{merchant}', 'MerchantsController@edit')->name('edit');
        //Deletes the current merchant
        Route::delete('/delete/{merchant}', 'MerchantsController@destroy')->name('destroy');

    });
});

Route::get('/duplicates/{item}', 'IndexController@duplicate')->name('duplicates');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
