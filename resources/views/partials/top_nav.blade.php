<header class="site-header navbar-nav">
    <!-- Topbar-->
    <div class="topbar d-flex justify-content-between" style="border-bottom: 1px solid gray; padding-bottom: 5px;">
        <!-- Logo-->
        <div class="container">
            <div class="row">
                <style>
                    #logo-:hover {
                        cursor: pointer;
                    }
                </style>
                <div class="d-flex col-lg-6 col-md-4 col-sm-12 col-xs-4">
                    <img src="{{asset('images/img/logo/weflycheaplogo.svg')}}"
                         class="img-responsive col-sm-12 col-xs-12" id="logo-"
                         style="width:auto; top: 15px;" alt="Unishop" onclick="location.href='/'">
                </div>
                <style>
                    @media only screen and (max-width: 375px) {
                        .search_box {
                            margin-left: 50px;
                        }
                    }

                    @media only screen and (min-width: 376px) {
                        .search_box {
                            margin-left: 55px;
                        }
                    }

                    @media only screen and (min-width: 576px) {
                        .search_box {
                            margin-left: 0px;
                        }
                    }
                </style>
                <div class="col-lg-6 col-md-8 col-sm-12 col-xs-4 search_box">
                    <form action="" class="input-group" method="GET" id="myform" role="search" onsubmit="return false;"
                          STYLE="width:auto; top: 15px;">
                        <div class="form-group">
                            <input type="text" id="keyword" class="col-lg-6 col-md-12 col-xs-12" name="keyword" autocapitalize="off"
                                   value="{{$keyword}}"
                                   placeholder="Search"
                                   style="
                                   padding-left: 5px;
                                   width: auto;
                                    height: 30px;
                                    border:none; border-bottom: 1px solid  #ffa251;
                                    text-transform: lowercase;
">

                            <input type="hidden" value="{{$keyword}}" name="originalKeyword">
                                <button
                                        class="btn btn-primary col-lg-6 col-md-3 col-sm-4 col-xs-4"
                                        style=" width: 40px; height: 30px; display: {{!empty($keyword) ? 'inline-block' :'none'}}"
                                        value="emptySearch"
                                        id="emptySearch"
                                        name="emptySearch">
                                    <p style="height: 45px; margin-right: auto; margin-left: -3px; margin-top: 1px;">
                                        X</p>
                                </button>
                                <button type="submit" class="btn btn-primary col-lg-6 col-md-3 col-sm-4 col-xs-4"
                                        style=" width: 40px; height: 30px; display: {{empty($keyword) ? 'inline-block' : 'none' }}"
                                        value="Filter"
                                        id="doSearch"
                                        form="myform" data-toggle="tooltip" data-placement="bottom" title="Search">
                                    <i class="fas fa-search"
                                       style="height: 45px; margin-right: auto; margin-left: -3px; margin-top: 5px;"></i>
                                </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Toolbar ( Put toolbar here only if you enable sticky navbar )-->
</header>
<style>
    @media only screen and (max-width: 0px ) {
        body {
            background-color: red;
        }
    }

    #nav {
        margin-top: 0;
        margin-bottom: 5px;
        border: none;
        /*border-bottom: 1px solid gray;*/
        padding: 0;
    }

    .nav-item {
        width: auto;
        padding-left: 5px;
        margin-right: 5px;
    }

    .nav-link {
        color: #184878;
        text-decoration: none;
        /*font-weight: bolder;*/
        font-size: 16px;
    }

    .nav-link:hover {
        color: red;
    }
</style>


<nav role="navigation" class="navbar navbar-inverse navbar-fixed-top justify-content-center"
     style="display: flex !important;">
    <ul class="nav justify-content-center" id="navbarNav">
        <li class="nav-item">
            <a class="nav-link active" href="{{route('index')}}">Home</a>
        </li>
        <li style="padding-top: 8px; color: gray;">
            |
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('merchants')}}">Aanbieders</a>
        </li>
        <li style="padding-top: 8px; color: gray;">
            |
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('brands')}}">Merken</a>
        </li>
    </ul>
</nav>

@section('scripts')
    @parent
    <script>


        {{--$("#emptySearch").click(function () {--}}
            {{--if($('#keyword').val() === '{{$keyword}}')--}}
            {{--{--}}
                {{--$('#keyword').val('');--}}
                {{--{{$keyword}}--}}
            {{--} else if ($('#keyword').val() != '{{$keyword}}' )--}}
            {{--{--}}
                    {{--$('#myform').submit();--}}
            {{--}--}}
        {{--});--}}

    </script>
@stop
