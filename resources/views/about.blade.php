@extends('layouts.contact_layout')

@section('content')
    <style>
        h3 {
            margin-top: 20px;
        }

        p {
            margin-bottom: 10px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                <h1 class="text-center text-bold">Over ons</h1>
                <p>
                    De website weflycheapvergelijker.nl is een initiatief waarbij gebruikers eenvoudig producten van
                    verschillende aanbieders kunnen vergelijken.
                </p>
            </div>
        </div>
    </div>
@stop
