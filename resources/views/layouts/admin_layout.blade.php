<!DOCTYPE html>
<html lang="en">

@include('partials.head')

<body>


@yield('admin_content')

<div class="container">
    @yield('admin_forms_content')
</div>

<script src="{{asset('js/3rdparty/vendor.min.js')}}"></script>
<script src="{{asset('js/3rdparty/card.min.js')}}"></script>
<script src="{{asset('js/3rdparty/modernizr.min.js')}}"></script>
<script src="{{asset('js/3rdparty/scripts.min.js')}}"></script>

@yield('scripts')
</body>

</html>