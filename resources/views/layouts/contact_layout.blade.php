<!DOCTYPE html>
<html lang="en">

@include('partials.head')

<body>

@include('partials.top_nav')

<div class="container-fluid">
    @yield('content')
</div>
<footer class="site-footer" style="background-image: url(img/footer-bg.png); padding-top: 50px !important; padding-bottom: 50px !important; bottom: 0;
position: absolute; width: 100%;
">
    <div class="container" style="margin-bottom: 0;">
        @yield('footer')
        <style>
            .nav-link2 {
                color: whitesmoke;
                font-size: 14px;
            }

            .nav-link3 {
                color: whitesmoke;
                text-decoration: none;
                /*font-weight: bolder;*/
                font-size: 14px;
            }

            .nav-link:hover {
                color: #f37100;
            }
        </style>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-2">
                <a href="{{url('/about')}}" class="nav-link2">Over ons</a>
                <br>
                <a href="{{url('/privacy-policy')}}" class="nav-link2">Privacy verklaring</a>
            </div>
            <div class="col-lg-3 nav-link3" style="line-height: 1.2;">
                We Fly Cheap <br>
                Assiesplein 1A <br>
                1233 RE Zwolle <br>
                038 20 57 03<br>
                info@weflycheap.nl

            </div>
            <div class="col-md-3"></div>
        </div>
        <!-- Copyright-->
        <p class="footer-copyright text-center" style="color: whitesmoke !important;">
            © 2020 We Fly Cheap B.V. - All rights reserved
        </p>
    </div>
</footer>
<script src="{{asset('js/3rdparty/vendor.min.js')}}"></script>
<script src="{{asset('js/3rdparty/card.min.js')}}"></script>
<script src="{{asset('js/3rdparty/modernizr.min.js')}}"></script>
<script src="{{asset('js/3rdparty/scripts.min.js')}}"></script>
@yield('scripts')
</body>

</html>
