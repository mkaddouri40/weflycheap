{{--<input type="submit" style="display: none" form="myform"/>--}}
<script>
    $(document).ready(function () {
        {{--$("#sortingby").change(function () {--}}
            {{--$("#brand_letter").val("{{$letter}}");--}}
            {{--$("#ascordesc").val($(this).val());--}}
            {{--document.forms["myform"].submit();--}}
        {{--});--}}

        {{--$("#keyword").submit(function () {--}}
            {{--$("#ascordesc").val($(this).val());--}}
            {{--document.forms["myform"].submit();--}}
        {{--});--}}

        {{--$(".submitter").click('li', function () {--}}
            {{--$("#brand_letter").val($(this).html());--}}
            {{--$("#keyword").val("{{$keyword}}");--}}
            {{--document.forms["myform"].submit();--}}
        {{--});--}}

        $('#doSearch').on('click', function (event) {
            document.forms["myform"].submit();
        });

        $('#dofilter').on('click',function (event) {
            document.forms["myform"].submit();
        });

        $('#min_price').on('input', function (e) {
            if ('{{$minPrice}}' !== 0) {
                if ($('#min_price').val() !== '{{$minPrice}}') {
                    $('#wisfilter').css("display", "none");
                    $('#dofilter').css("display", "inline-block");
                } else {
                    $('#wisfilter').css("display", "inline-block");
                    $('#dofilter').css("display", "none");
                }
            }
        });

        $('#max_price').on('input', function (e) {
            if (parseInt({{$maxPrice}}) !== parseInt({{$maxMaxPrice}})) {
                if ($('#max_price').val() !== '{{$maxPrice}}') {
                    $('#wisfilter').css("display", "non0e");
                    $('#dofilter').css("display", "inline-block");
                } else {
                    $('#wisfilter').css("display", "inline-block");
                    $('#dofilter').css("display", "none");
                }
            }
        });

        $('#min_price, #max_price').on('input', function (e) {
            if ('{{$minPrice}}' !== 0 || parseInt({{$maxPrice}}) !== parseInt({{$maxMaxPrice}})) {
                if ($('#min_price').val() !== '{{$minPrice}}' || $('#max_price').val() !== '{{$maxPrice}}') {
                    $('#wisfilter').css("display", "none");
                    $('#dofilter').css("display", "inline-block");
                } else {
                    $('#wisfilter').css("display", "inline-block");
                    $('#dofilter').css("display", "none");
                }
            }
        });

        $(window).keydown(function (event) {
            if (event.keyCode === 13) {
                event.preventDefault();

                if (event.target.id === "keyword") {
                    document.forms["myform"].submit();
                }
                if (event.target.id === "min_price") {
                    document.forms["myform"].submit();
                }
                if (event.target.id === "max_price") {
                    document.forms["myform"].submit();
                }
                if (event.target.id === "sortby") {
                    document.forms["myform"].submit();
                }
            }
        });


        $('#emptySearch').on('click', function (event) {
            event.preventDefault();

            if ($('#keyword').val() !== '' && '{{$keyword}}' !== '') {
                $('#keyword').val('');
                parseInt($('#min_price').val(0));
                $('#max_price').val({{$maxMaxPrice}});
                $('#sortbyprice').val('sorteer op');
                document.forms["myform"].submit();
                // parseInt($('#min_price').val(0));
                // event.preventDefault();
                // keyword = '';
            }
            // document.forms["myform"].submit();
        });

        $('#wisfilter').click(function (event) {
            event.preventDefault();

            if (parseInt($('#min_price').val()) !== 0 && parseInt($('#max_price').val()) !== parseInt({{$maxMaxPrice}})) {
                parseInt($('#min_price').val(0));
                $('#max_price').val({{$maxMaxPrice}});
                $('#sortbyprice').val('sorteer op');
                document.forms["myform"].submit();
            }
            if (parseInt($('#min_price').val()) !== 0 && parseInt($('#max_price').val()) === parseInt({{$maxMaxPrice}})) {
                parseInt($('#min_price').val(0));
                $('#max_price').val({{$maxMaxPrice}});
                $('#sortbyprice').val('sorteer op');
                document.forms["myform"].submit();
            }
            if (parseInt($('#max_price').val()) !== parseInt({{$maxMaxPrice}}) && parseInt($('#min_price').val()) === 0) {
                parseInt($('#min_price').val(0));
                $('#max_price').val({{$maxMaxPrice}});
                $('#sortbyprice').val('sorteer op');
                document.forms["myform"].submit();
            }
        });

    });


</script>
