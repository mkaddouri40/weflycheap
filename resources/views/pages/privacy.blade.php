@extends('layouts.layout')

@section('content')
    <div class="container padding-bottom-2x mb-2">
        <h2>Verklaring over privacy en cookies op weflycheapvergelijker.nlvergelijk.nl
            Persoonsgegevens die worden verwerkt</h2>

        <h3>Persoonsgegevens die worden verwerkt</h3>
        <p>
            We Fly Cheap kan persoonsgegevens over u verwerken, doordat u gebruik maakt van de diensten van
            We Fly Cheap, en/of omdat u deze zelf bij het invullen van een contactformulier op de website aan
            We Fly Cheap verstrekt. We Fly Cheap kan de volgende persoonsgegevens verwerken:
        </p>
        <small>
            Uw voor- en achternaam

            Uw adresgegevens
            Uw telefoonnummer
            Uw e-mailadres
        </small>
        <style>
            h3 {
                margin-top: 20px;
            }

            p {
                margin-bottom: 10px;
            }
        </style>
        <h3> Waarom We Fly Cheap Gegevens nodig heeft</h3>
        <p>
            We Fly Cheap verwerkt uw persoonsgegevens om telefonisch contact met u op te kunnen nemen als u daar
            om verzoekt, en/of om u schriftelijk (per e-mail en/of per post) te kunnen benaderen indien u telefonisch
            onverhoopt niet bereikt kunt worden. Daarnaast kan We Fly Cheap uw persoonsgegevens gebruiken in het
            kader van het uitvoeren van een met u gesloten overeenkomst, doorgaans de levering van diensten.
        </p>

        <h3> Hoe lang We Fly Cheap gegevens bewaart</h3>
        <p>
            We Fly Cheap bewaart uw persoonsgegevens niet langer dan strikt nodig is om de doelen te realiseren,
            waarvoor uw gegevens worden verzameld. Uw gegevens worden niet langer dan een jaar bewaard indien er geen
            overeenkomst met u tot stand komt.
        </p>

        <h3> Delen met anderen</h3>
        <p>
            We Fly Cheap verstrekt uw persoonsgegevens alléén aan derden indien dit nodig is voor de uitvoering
            van een overeenkomst met u, of om te voldoen aan een wettelijke verplichting of, indien u hier toestemming
            voor heeft gegeven.
        </p>

        <h3> Inzage, correctie, wijzigen en verwijderen van jouw gegevens</h3>
        <p>
            Je hebt het recht je gegevens in te zien, te verwijderen en/of te wijzigen. Je kunt hiervoor contact opnemen
            met We Fly Cheap via de volgende gegevens:

            Telefonisch: 035 691 22 37
            E-mail: administratie@weflycheap.nl
            Post: AssiesPlein 1a, 2045 RN Zwolle, T.a.v. We Fly Cheap
        </p>

        <h3> Klachten</h3>
        <p>
            U kunt schriftelijk een klacht indienen bij de autoriteit persoonsgegevens.
            U kunt dit doen door een brief te sturen naar Autoriteit Persoonsgegevens, Postbus 93374, 2509 AJ in Den
            Haag
            Vermeld dan altijd in uw brief:
            <br><br>
            <small>
                · Over welke persoonsgegevens het gaat

                · Over welke organisatie het gaat

                · Of u al contact heeft gehad met die organisatie

                · Hoe de organisatie reageerde op uw klacht

                · Of (het negatieve gevolg van) de verwerking nog plaatsvindt

                · Wat u met de klacht wilt bereiken

                · Uw naam, adres en telefoonnummer
            </small>
            Twijfelt u of u een klacht wilt of kunt indienen bij de AP, belt u dan eerst via het gratis telefoonnummer
            088 - 1805 250.
        </p>

        <h3> Privacy wetgeving en Analytics</h3>
        <p>
            Zoals hieronder beschreven in de officiële verklaring die Google Analytics gebruikers op hun website moeten
            plaatsen maakt deze website gebruik van Google Analytics. Dit programma levert belangrijke statistieken over
            aantallen bezoekers, de bezochte pagina’s, de verkeersbronnen en andere essentiële informatie waarmee wij
            deze website voortdurend kunnen verbeteren voor de gebruikers. Deze statistieken zijn anoniem en niet te
            herleiden tot een bepaalde persoon of gebruiker. Er wordt geen gebruik gemaakt van ‘behavioural targeting’
            waarmee u op basis van uw klikgedrag speciale inhoud krijgt te zien; uw surfgedrag blijft volledig anoniem.
            Gegevens uit contactformulieren worden nooit in Analytics verwerkt of opgeslagen. Google Analytics maakt
            gebruik van een cookie, dat is een klein bestandje dat op uw computer wordt opgeslagen. Wij hebben Google
            Analytics conform de privacy wetgeving ingericht, zodat er geen toestemming vooraf van de gebruiker nodig
            is.
        </p>

        <h3> IP adressen worden gemaskeerd en gegevens niet gedeeld</h3>
        <p>
            Het cookie van Google Analytics is een ‘first party cookie’, dat wil zeggen dat het cookie officieel
            eigendom is van We Fly Cheap en niet van Google. Dit cookie wordt niet gebruikt voor advertenties of
            andere commerciële doeleinden. We Fly Cheap heeft een verwerkersovereenkomst met Google. Daarnaast
            is Google Analytics zo ingesteld dat IP-adressen anoniem zijn gemaakt, zodat gegevens nooit naar u of uw IP
            adres te herleiden zijn. Verder wordt vanuit het Analytics account van We Fly Cheap geen informatie
            gedeeld met Google of andere partijen. Daarmee is het Analytics cookie van We Fly Cheap een
            niet-privacy gevoelig first-party cookie dat nodig is om de kwaliteit en veiligheid van rens-en-rens.nl te
            kunnen waarborgen. Het Analytics cookie blijft 6 maanden geldig.
        </p>

        <h3> Opt-out</h3>
        <p>
            Als u het script van Analytics liever helemaal wilt blokkeren dan kunt u hier een opt-out plugin voor Google
            Analytics downloaden: https://tools.google.com/dlpage/gaoptout. Met deze plugin telt uw bezoek niet meer mee
            in de statistieken van alle websites die Google Analytics gebruiken.
        </p>
        <h3>
            Google Analytics – verklaring van Google
        </h3>
        <p>
            Deze website maakt gebruik van Google Analytics, een webanalyse-service die wordt aangeboden door Google
            Inc. (“Google”). Google Analytics maakt gebruik van “cookies” (tekstbestandjes die op uw computer worden
            geplaatst) om de website te helpen analyseren hoe bezoekers de site gebruiken. De door het cookie
            gegenereerde anonieme informatie over uw gebruik van de website wordt overgebracht naar en door Google
            opgeslagen op servers in de Verenigde Staten. Google gebruikt deze informatie om bij te houden hoe deze
            website wordt gebruikt, rapporten over de website-activiteit op te stellen voor de eigenaar van deze website
            (Smart IM BV) en andere diensten aan te bieden met betrekking tot website-activiteit en internetgebruik.
            Google mag deze informatie alleen aan derden verschaffen indien Google hiertoe wettelijk wordt verplicht, of
            voor zover deze derden de informatie namens Google verwerken. Google zal Uw IP-adres niet combineren met
            andere gegevens waarover Google beschikt. U kunt het gebruik van cookies weigeren door in Uw browser de
            daarvoor geëigende instellingen te kiezen. Wij wijzen u er echter op dat u in dat geval wellicht niet alle
            mogelijkheden van deze website kunt benutten. Door gebruik te maken van deze website geeft u toestemming
            voor het verwerken van de informatie door Google op de wijze en voor de doeleinden zoals hiervoor
            omschreven.
        </p>

        <h3>
            Cookieverklaring
        </h3>
        <p>
            We Fly Cheap maakt gebruik van cookies. We gebruiken cookies om content en advertenties te
            personaliseren, om functies voor social media te bieden en om ons websiteverkeer te analyseren. Ook delen we
            informatie over uw gebruik van onze site met onze partners voor social media, adverteren en analyse. Deze
            partners kunnen deze gegevens combineren met andere informatie die u aan ze heeft verstrekt of die ze hebben
            verzameld op basis van uw gebruik van hun services.
            <br><br>
            Cookies zijn kleine tekstbestanden die door websites kunnen worden gebruikt om gebruikerservaringen
            efficiënter te maken.
            <br><br>
            Volgens de wet mogen wij cookies op uw apparaat opslaan als ze strikt noodzakelijk zijn voor het gebruik van
            de site. Voor alle andere soorten cookies hebben we uw toestemming nodig.
            <br><br>
            Deze website maakt gebruik van verschillende soorten cookies. Sommige cookies worden geplaatst door diensten
            van derden die op onze pagina's worden weergegeven.
            <br><br>
            Uw toestemming geldt voor de volgende gebieden: www.weflycheapvergelijker.nl
        </p>
    </div>



@stop
