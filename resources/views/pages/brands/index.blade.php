@extends('layouts.layout')

@section('content')
    {{--@if(isset($details))--}}
    <section class="container padding-bottom-2x mb-2" style="margin-top: 20px;">
        {{Breadcrumbs::render('brands')}}
        <br>
        <style>
            .submitter {
                display: inline-block;
                margin-right: 5px;
                border: none;
                background-color: transparent;
                color: #184878;
                font-size: 16px;
                text-decoration: none;
            }

            .submitter:hover {
                cursor: pointer;
                color: #f37100;
            }

            .submitter.active {
                color: #f37100;
                font-weight: bold;
                cursor: default;
                pointer-events: none;
            }

            .search-letters {
                border-top: 1px solid #f37100;
                padding-top: 10px;
                color: #184878;
                width: fit-content;
            }
        </style>
        <p style="margin-left: 100px; display: block"> Zoek op letter</p>
        <div class="search-letters col-lg-10 col-md-2 col-sm-2 col-xs-1" style="margin-left: 100px;">
            <ul>
                <li class="submitter" style="margin-right: 8px; text-decoration: underline;">*</li>
                <li class="submitter @if($letter == 'a')active @endif">a</li>
                <li class="submitter @if($letter == 'b')active @endif">b</li>
                <li class="submitter @if($letter == 'c')active @endif">c</li>
                <li class="submitter @if($letter == 'd')active @endif">d</li>
                <li class="submitter @if($letter == 'e')active @endif">e</li>
                <li class="submitter @if($letter == 'f')active @endif">f</li>
                <li class="submitter @if($letter == 'g')active @endif">g</li>
                <li class="submitter @if($letter == 'h')active @endif">h</li>
                <li class="submitter @if($letter == 'i')active @endif">i</li>
                <li class="submitter @if($letter == 'j')active @endif">j</li>
                <li class="submitter @if($letter == 'k')active @endif">k</li>
                <li class="submitter @if($letter == 'l')active @endif">l</li>
                <li class="submitter @if($letter == 'm')active @endif">m</li>
                <li class="submitter @if($letter == 'n')active @endif">n</li>
                <li class="submitter @if($letter == 'o')active @endif">o</li>
                <li class="submitter @if($letter == 'p')active @endif">p</li>
                <li class="submitter @if($letter == 'q')active @endif">q</li>
                <li class="submitter @if($letter == 'r')active @endif">r</li>
                <li class="submitter @if($letter == 's')active @endif">s</li>
                <li class="submitter @if($letter == 't')active @endif">t</li>
                <li class="submitter @if($letter == 'u')active @endif">u</li>
                <li class="submitter @if($letter == 'v')active @endif">v</li>
                <li class="submitter @if($letter == 'w')active @endif">w</li>
                <li class="submitter @if($letter == 'x')active @endif">x</li>
                <li class="submitter @if($letter == 'y')active @endif">y</li>
                <li class="submitter @if($letter == 'z')active @endif">z</li>
                <select id="sortingby" form="myform" class="col-lg-2 col-md-2 col-sm-2 col-xs-1"
                        style="border:none; border-bottom: 1px solid  #ffa251; background-color: transparent; color: gray; font-size: 15px; margin-left:0px;">
                    <option value="sorteer op">@if($sorteringmerk != '') {{$sorteringmerk}} @else sorteer
                        op @endif</option>
                    <option value="merk oplopend" @if($sorteringmerk == "merk oplopend") selected @endif>merk oplopend
                    </option>
                    <option value="merk aflopend" @if($sorteringmerk == "merk aflopend") selected @endif>merk aflopend
                    </option>
                </select>
            </ul>
            <input type="hidden" form="myform" value="{{$letter}}" name="brand_letter" id="brand_letter">
            <input type="hidden" form="myform" value="{{$sorteringmerk}}" name="ascordesc" id="ascordesc">

        </div>
        <div class="row">

            <br>
            <style>
                .text-list-l {
                    color: #184878;
                    text-decoration: none;
                    font-weight: bolder;
                    font-size: 13px;
                }

                .text-list-l:hover {
                    color: #f37100;
                }
            </style>
            <div class="col-lg-12" style="line-height: 1.7; margin-left: 25%; margin-right: auto;">
                @foreach($items as $item)
                    <a class="text-list-l" href="{{ route('brand', $item->brand)}}">
                        {{$item->brand}}
                    </a><br>
                @endforeach
            </div>
        </div>
        {{--        {{$items->links()}}--}}
    </section>
@stop


@section('footer')
@stop
@section('scripts')
    <script>
    $("#sortingby").change(function () {
    $("#brand_letter").val("{{$letter}}");
    $("#ascordesc").val($(this).val());
    document.forms["myform"].submit();
    });

    $("#keyword").submit(function () {
    $("#ascordesc").val($(this).val());
    document.forms["myform"].submit();
    });

    $(".submitter").click('li', function () {
    $("#brand_letter").val($(this).html());
    $("#keyword").val("{{$keyword}}");
    document.forms["myform"].submit();
    });
</script>

@include('scripts')
@stop