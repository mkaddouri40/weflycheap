@extends('layouts.contact_layout')

@section('content')
    <div class="container padding-bottom-2x mb-2" style="margin-top: 140px;">
        <div class="row">
            <div class="col-md-7">
            </div>
            <div class="col-md-5">
            </div>
        </div>
        <hr class="margin-top-2x">
        <div class="row margin-top-2x">
            <div class="col-md-7">
                <div class="display-3 text-muted opacity-75 mb-30">Contact</div>
            </div>
            <div class="col-md-5">
                <ul class="list-icon">
                    <li><a class="navi-link" href="mailto:support@unishop.com">info@smart-vergelijk.nl</a></li>
                    <li>035 - 33 38 39</li>
                    <li>1 - 2 business days</li>
                </ul>
            </div>
        </div>
        <hr class="margin-top-2x">


    </div>
@stop