@extends('layouts.layout')


@section('content')
        <section class="container padding-bottom-2x mb-2">
            <h2 class="h3 pb-3 text-center">Products filtered on brand</h2>
            <div class="row">
                @foreach($brands as $item)
                    @php($image = $item->images->first())
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="product-card mb-30">
                            <div class="product-card-body">
                                <a class="product-thumb" href="{{route('show',$item->id)}}"><img
                                            src="{{$image->location}}"
                                            alt="Product" class="img-responsive" STYLE="height: 345px"></a>
                                <h3 class="product-title"><a href="shop-single.html">{{$item->title}}</a></h3>
                                <h4 class="product-price">
                                    {{$item->price}}
                                </h4>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">{{ $brands->links() }}</li>
            </ul>
        </nav>
@stop


@section('footer')
    <hr class="hr-light mt-2 margin-bottom-2x hidden-md-down">
    <!-- Copyright-->
    <p class="footer-copyright text-center">
        © All rights reserved
    </p>
    <p class="footer-copyright text-center">
        Address: Arendstraat 33, 1223 RE Hilversum
    </p>
@stop

