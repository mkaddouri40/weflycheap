@extends('layouts.single_item_layout')
<style>
    /* The Modal (background) */
    .modal {
        display: none;
        position: fixed;
        z-index: 1;
        padding-top: 100px;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: black;
    }

    /* Modal Content */
    .modal-content {
        position: relative;
        background-color: #fefefe;
        margin: auto;
        padding: 0;
        width: 90%;
        max-width: 600px;
    }

    /* The Close Button */
    .close {
        color: white;
        position: absolute;
        top: 10px;
        right: 25px;
        font-size: 35px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #999;
        text-decoration: none;
        cursor: pointer;
    }

    /* Hide the slides by default */
    .mySlides {
        display: none;
    }

    /* Next & previous buttons */
    .prev,
    .next {
        cursor: pointer;
        position: absolute;
        top: 50%;
        width: auto;
        padding: 16px;
        margin-top: -50px;
        color: white;
        font-weight: bold;
        font-size: 20px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
        user-select: none;
        -webkit-user-select: none;
    }

    /* Position the "next button" to the right */
    .next {
        right: 0;
        border-radius: 3px 0 0 3px;
    }

    /* On hover, add a black background color with a little bit see-through */
    .prev:hover,
    .next:hover {
        background-color: rgba(0, 0, 0, 0.8);
    }

    /* Number text (1/3 etc) */
    .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
    }

    /* Caption text */
    .caption-container {
        text-align: center;
        background-color: black;
        padding: 2px 16px;
        color: white;
    }

    img.demo {
        opacity: 0.6;
    }

    .active,
    .demo:hover {
        opacity: 1;
    }

    img.hover-shadow {
        transition: 0.3s;
    }

    .hover-shadow:hover {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
</style>
@section('content')
    <!-- Page Content-->
    <div class="container padding-bottom-3x" style="margin-top: auto;">
        {{Breadcrumbs::render('item',$item)}}
        <div class="row">

            <!-- Poduct Gallery-->
            <div class="col-md-6">
                <div class="product-gallery">
                    <div class="gallery-wrapper">
                    </div>
                    <div class="product-carousel owl-carousel gallery-wrapper">
                        @php($first=true)
                        @foreach( $images as $image )
                            <div class="{{ $first ? 'active' : '' }}" data-hash="{{ $image->id }}">
                                <a href="{{ $image->id }}">
                                    <img src="{{ $image->location }}"
                                         style=" width:auto; margin-left: auto; margin-right:auto; max-height: 600px;  width: auto !important;"
                                         alt="{{$item->title}}"
                                         id="myImg"
                                         onclick="openModal();currentSlide({{$image->id}})">
                                </a>
                            </div>
                            @php($first=false)
                        @endforeach
                    </div>
                    <ul class="product-thumbnails">
                        @php($first=true)
                        @foreach( $images as $index => $image )
                            <li class=" active{{ $first ? 'active' : '' }}">
                                <a href="#{{ $image->id }}" style="border: 1px solid black;">
                                    <img src="{{$image->location}}" style="height: 100px; width: auto !important;"
                                         alt="{{$item->title}}">
                                </a>
                            </li>
                            @php($first=false)
                        @endforeach
                    </ul>
                </div>
            </div>
            <!-- Product Info-->
            <div class="col-md-6">
                <h2 style="margin-top: 24px;" class="mb-3">{!! $item->title !!}</h2><span class="h3 d-block">
                    </span>
                <p class="text-muted">{{$item->short_description}} <a href='#details'></a></p>
                <hr class="mb-2">
                <style>
                    @media only screen and (max-width: 375px) {
                        .table_meer_aanbieders {
                            width: 350px;
                        }
                    }
                </style>
                <p style="margin-top: 15px;"> Merk: <strong> {{$item->brand}}</strong></p>
                <div class="table-responsive table_meer_aanbieders" style="margin-top: 30px;">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Aanbieder</th>
                            <th>Prijs</th>
                            <th>Product link</th>
                            <th>Beschikbaarheid</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @php($merchant = $item->merchant)
                            <td>{{$merchant->name}}</td>
                            <td>&euro;&nbsp;{{str_replace('.',',',$item->price)}}</td>
                            <td>
                                <button class="btn btn-primary"
                                        style="width: auto; height: 22px; margin-top: -5px;"
                                        onclick="window.location='{{$item->link}}'">
                                    <p style="margin-bottom: 0; font-size: smaller;  font-weight: bold;">
                                        Bekijk
                                    </p>
                                </button>
                            </td>
                            <td>{{$item->availability}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal">
        <span class="close cursor" onclick="closeModal()">&times;</span>
        <div class="modal-content">

            <div class="mySlides">
                <div class="numbertext"></div>
                <img src="{{$image->location}}" style="width:100%">
            </div>

            <!-- Caption text -->
            <div class="caption-container">
                <p id="caption">{{$item->title}}</p>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal">
        <!-- The Close Button -->
        <span class="close">&times;</span>
        <!-- Modal Content (The Image) -->
        <img class="modal-content" id="img01">
        <!-- Modal Caption (Image Text) -->
        <div id="caption"></div>
    </div>
@stop

@section('footer')

@stop

@section('scripts')
    <script>
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById('myImg');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function () {
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function () {
            modal.style.display = "none";
        }
        $('body').keydown(function(event){
            if(event.keyCode == 27){
                // redirect
                modal.style.display = "none";
            }
        });
        $('body').keydown(function (event) {
            if(event.keyCode == 70) {
                modal.style.display = "none";
            }
        });
        // $('body').ontouchstart(function (event) {
        //     modal.style.display = "none";
        // });
        document.addEventListener('touchstart', function(e) {
            e.preventDefault();
            var touch = e.touches[0];
           modal.style.display = "none";
        }, false);

        $('body').mousedown(function () {
            modal.style.display = "none";
        });



    </script>
    <script>
        // Open the Modal
        function openModal() {
            document.getElementById('myModal').style.display = "block";
        }

        // Close the Modal
        function closeModal() {
            document.getElementById('myModal').style.display = "none";
        }

        var slideIndex = 1;
        showSlides(slideIndex);

        // Next/previous controls
        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        // Thumbnail image controls
        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("demo");
            var captionText = document.getElementById("caption");
            if (n > slides.length) {
                slideIndex = 1
            }
            if (n < 1) {
                slideIndex = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";
            captionText.innerHTML = dots[slideIndex - 1].alt;
        }
    </script>
    <script>
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg1');
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    img.onclick = function () {
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
    modal.style.display = "none";
    }
    </script>
@stop
