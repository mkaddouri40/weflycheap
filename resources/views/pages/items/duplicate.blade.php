@extends('layouts.single_item_layout')

@section('content')
    <!-- Page Content-->
    <div class="container padding-bottom-3x">
        <div class="row">

            <div class="col-md-6">

                <!-- Poduct Gallery-->
                <div class="product-gallery">
                    <div class="gallery-wrapper">
                    </div>
                    <div class="product-carousel owl-carousel gallery-wrapper">
                        @php($first=true)
                        @foreach( $images as $image )
                            <div class="gallery-item {{ $first ? 'active' : '' }}" data-hash="one">
                                <a href="{{ $image->id }}" data-size="1000x667">
                                    <img style="height: 400px;" src="{{ $image->location }}" alt="Product">
                                </a>
                            </div>
                            @php($first=false)
                        @endforeach
                    </div>

                </div>
            </div>
            <!-- Product Info-->
            <div class="col-md-6">

                <h2 class="mb-3">{{$item->title}}</h2><span class="h3 d-block">{{$item->price}}</span>
                <p class="text-muted">{{$item->short_description}} <a href='#details' class='scroll-to'>More
                        info</a></p>
                <div class="pt-1 mb-4"><span class="text-medium">SKU:</span>{{$item->sku}}</div>
                <div class="pt-1 mb-4"><span class="text-medium">Availability:</span>{{$item->availability}}</div>
                <div class="pt-1 mb-4">
                    <a href="{{$item->link}}" class="text-medium">
                        Visit page for this product
                    </a>

                </div>
                <hr class="mb-2">
            </div>
        </div>
    </div>
    <!-- Product Details-->
    <div class="bg-secondary padding-top-3x padding-bottom-2x mb-3" id="details">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="h4">Details</h3>
                    <p class="mb-4">{{$item->description}}.</p>
                </div>
                <div class="col-md-6">

                    @if (count($items) > 1)
                        <h3 class="h4">Check out product from other resellers</h3>
                        <div class="row">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Merchant</th>
                                    <th>Price</th>
                                    <th>Brand</th>
                                    <th>Where to buy</th>
                                </tr>
                                </thead>
                                @foreach ($items as $compared_item)
                                    <tbody>
                                    <tr>
                                        <td>MErchants name komt hier</td>
                                        <td>{{$compared_item->price}}</td>
                                        <td>{{$compared_item->brand}}</td>
                                        <td><button class="btn btn-success"  onclick="location.href='{{$compared_item->link}}'">BUY!</button></td>
                                    </tr>
                                    </tbody>
                                @endforeach
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop