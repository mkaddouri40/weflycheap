@extends('layouts.layout')


@section('content')
    <style>
        .input_price:before {
            content: '1';
        }
    </style>
    {{--@if(isset($details))--}}
    <section class="container padding-bottom-2x mb-2" style="margin-top: 20px;">
        {{Breadcrumbs::render('items', $merchant)}}
        {{--        <h2 class="h3 pb-3 text-center">Results for: {{$query}}</h2>--}}
        <span>Prijsfiltering</span>
        <div class="row" style="margin-left: 2px;">
            <input type="number" maxlength="4" step="1" class="input_price col-lg-1 col-md-2 col-sm-4 col-xs-1"
                   form="myform"
                   min="0"
                   name="min_price"
                   id="min_price"
                   placeholder="EUR"
                   style="color: gray; font-size: 15px;  border:none; border-bottom: 1px solid  #ffa251;"
                   value="{{$minPrice}}">
            <div id="verbindings-streep" class="d-none d-lg-block d-md-block"
                 style="display: block; padding-top: 10px; margin-left: 15px; margin-right: 15px; font-weight: bold; font-size: 18px;">
                -
            </div>
            <input type="number" step="1" class="input_price col-lg-1 col-md-2 col-sm-4 col-xs-1" form="myform"
                   placeholder="EUR"
                   max="{{$maxMaxPrice}}"
                   name="max_price"
                   id="max_price"
                   value="{{$maxPrice}}"
                   style="color: gray; font-size: 15px; border:none; border-bottom: 1px solid  #ffa251;">
            {{--<input type="hidden" value="{{$maxPrice}}" name="maxMaxPrice">--}}
            <button id="wisfilter"
                    class="btn btn-primary"
                    style=" height: 22px;
                            display: @if($minPrice != 0 || $maxPrice != $maxMaxPrice )  inline-block @else none @endif
                    {{--                        display: {{$minPrice > 0 ? 'inline-block' :'none'}}--}}
                            ">
                <p style="margin: -2px auto 0 auto; font-size: smaller; font-weight: bold;">
                    Wis
                </p></button>
            <button type="submit"
                    class="btn btn-primary"
                    form="myform"
                    id="dofilter"
                    style=" height: 22px;
                            display: @if($minPrice == 0 && $maxPrice == $maxMaxPrice) inline-block @else none @endif">
                <p style="margin: -2px auto 0 auto; font-size: smaller; font-weight: bold;">
                    Filter
                </p></button>
            <div style="padding-top: 10px;  margin-right: 15px; font-weight: bold; font-size: 18px; margin-left: 100px;"></div>
            <select form="myform" name="sortby" id="sortbyprice"
                    class="col-lg-2 col-md-2 col-sm-2 col-xs-1"
                    style="border:none; border-bottom: 1px solid  #ffa251; background-color: transparent; color: gray; font-size: 15px; font-weight: normal; ">
                {{--<option value="" class="form-control" >OPlopend</option>--}}
                <option value="{{$sortby ? $sortby : $previoussortby }}"
                        class="form-control">{{$sortby ? $sortby : $previoussortby }}</option>
                @if( $sortby === "prijs oplopend")  @else
                    <option value="prijs oplopend" class="form-control">prijs oplopend</option> @endif
                @if($sortby === "prijs aflopend") @else
                    <option value="prijs aflopend" class="form-control">prijs aflopend</option> @endif
                @if($sortby === "merk oplopend") @else
                    <option value="merk oplopend" class="form-control">merk oplopend</option> @endif
                @if($sortby === "merk aflopend") @else
                    <option value="merk aflopend" class="form-control">merk aflopend</option> @endif
            </select>
            <input form="myform" type="hidden" name="previoussortby" value="{{$sortby}}">
            <br>
        </div>
        <br>
        <div class="row">
            {{$items->links()}}
            @foreach($items as $item)
                @php($image = $item->images->first())
                <div class="col-lg-3">
                    @php($merchant = $item->merchant)
                    <div class="product-card mb-30" style="border-radius: 0px;">
                        <div class="product-card-body" style="height:300px; position: relative;">
                            <a class="product-category" href="{{route('brand', ['brand' => $item->brand])}}">{{$item->brand}}</a>
                            <a class="product-thumb" href="{{route('show',['slug_title' => $item->slug_title])}}"><img
                                        src="{{$image->location}}"
                                        alt="{{$item->title}}" class="img-responsive text-center"
                                        STYLE="height: 100px; margin-left: auto; margin-right: auto; width: auto; margin-top: 5px;"></a>
                            <h3 class="product-title"><a href="{{route('show', [ 'slug_title' => $item->slug_title])}}">{{$item->title}}</a></h3>
                            <div class="product-card mb-30"
                                 style="margin-bottom:0 !important; left: 0;bottom: 0px; position: absolute;  border-radius: 0px; border-color: transparent; border-top: 1px solid #ffa251; ">
                                @php($item_id = base64_encode($item->id))
                                <button class="btn btn-primary" style="margin-left: 10px; width: auto; height: 22px;"
                                        onclick="location.href='{{ route('show', [ 'slug_title' => $item->slug_title])}}'">
                                    <p style="margin-bottom: 15px; font-size: smaller;  font-weight: bold;">
                                        Bekijk
                                    </p>
                                </button>
                                <h4 class="product-price text-center" STYLE="top: 10PX;">
                                    &euro; {{str_replace('.',',',$item->price)}}
                                </h4>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <br>
        {{$items->links()}}
    </section>
@stop

@section('footer')
@stop
@section('scripts')
    <script>
        var select = document.getElementById('sortbyprice');
        select.addEventListener('change', function () {
            this.form.submit();
        }, false);
    </script>
    @include('scripts')
@stop