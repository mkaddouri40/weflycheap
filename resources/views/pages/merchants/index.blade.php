@extends('layouts.layout')


@section('content')
    {{--<div class="container padding-bottom-1x">--}}
        <div class="container padding-bottom-3x mb-2">
            <div class="container padding-bottom-2x mb-2">
                {{--@if(isset($details))--}}
                <section class="container padding-bottom-2x mb-2">
                    {{Breadcrumbs::render('merchants')}}
                    {{--        <h2 class="h3 pb-3 text-center">Results for: {{$query}}</h2>--}}
                    <br>
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="text-center">Aanbieders</h1>
                        </div>
                        <br>
                        @foreach($merchants as $merchant)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="margin-top: 10px;">
                                <div class="product-card mb-30" style="border-radius: 0px;">
                                    <div class="product-card-body" style="height:300px;">

                                        @php($name = str_slug($merchant->name))
                                        <a class="product-thumb" href="{{ route('items',['merchant' => $name])}}"><img
                                                    src="{{$merchant->logo}}"
                                                    alt="{{$merchant->title}}" class="img-responsive text-center"
                                                    STYLE="height: 100px; margin-left: auto; margin-right: auto; width: auto; margin-top: 20%;"></a>
                                        <h3 class="product-title"><a href="{{$merchant->link}}">{{$merchant->name}}</a>
                                        </h3>
                                        <div class="mb-30" style="left: 0;bottom: 0px; position: absolute;">
                                            <button class="btn btn-primary"
                                                    style=" height: 22px; width: auto; margin-left:60px; margin-right: 60px;"
                                                    onclick="location.href='{{ route('items', ['merchant' => $name])}}'">
                                                <p style="margin-bottom: 15px; font-size: smaller; font-weight: bold;">
                                                    Bekijk
                                                </p>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </section>
            </div>
        </div>
    {{--</div>--}}
@stop


@section('footer')
@stop

@section('scripts')
    @include('scripts')
@stop