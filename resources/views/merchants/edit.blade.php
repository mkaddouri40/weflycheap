@extends('layouts.admin_layout')
@section('admin_forms_content')
    <div class="container">
        <h5>Edit Merchant</h5>
        <h1>{{request()->route('id')}}</h1>
        <form method="POST"
              action="{!! route('smartbeheer.merchants.store', ['merchant' => $merchant->id]) !!}">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name" class="cols-sm-2 control-label">Merchants name</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="name" id="name"
                           placeholder="Enter the merchants name" value="{{$merchant->name}}">
                </div>
            </div>

            <div class="form-group">
                <label for="description" class="cols-sm-2 control-label">Description</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="description" id="description"
                           placeholder="Enter a description about the merchant"
                           value="{{$merchant->description}}">
                </div>
            </div>

            <div class="form-group">
                <label for="logo" class="cols-sm-2 control-label">Upload a Logo</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="logo" id="logo"
                           placeholder="CLick here to upload a logo" value="{{$merchant->logo}}">
                </div>
            </div>

            <div class="form-group">
                <label for="website" class="cols-sm-2 control-label">Enter a website for the merchant</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="website" id="website"
                           placeholder="Enter the merchants website" value="{{$merchant->website}}">
                </div>
            </div>

            <div class="form-group">
                <label for="xml" class="cols-sm-2 control-label">Enter xml url for the merchant</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="feed_xml" id="xml"
                           placeholder="Enter xml url for the merchant" value="{{$merchant->feed_xml}}">
                </div>
            </div>

            <div class="form-group">
                <label for="logo" class="cols-sm-2 control-label">Enable the merchant</label>
                <div class="input-group">
                    <span class="input-group-addon"></span>
                    <select name="active" id="active" class="form-control">
                        <option value="0">Disable Merchant</option>
                        <option value="1">Enable Merchant</option>
                    </select>
                </div>
            </div>

            <div class="form-group ">
                <input type="submit" name="submit" id="button"
                       class="btn btn-success btn-lg btn-block login-button">
            </div>


        </form>
    </div>
@stop

