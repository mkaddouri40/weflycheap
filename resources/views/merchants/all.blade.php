@extends('layouts.admin_layout')
@section('admin_content')
    <div class="fresh-table full-color-azure full-screen-table" id="fresh-table">
        <div class="row">
            <ul class="text-center col-xs-6">
                <a class="btn btn-facebook" href="{{url('weflycheap/merchants/create')}}">ADD MERCHANTS</a>
            </ul>
            <h2 class="header text-center col-xs-6">
                We Fly Cheap
            </h2>
        </div>
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <table id="fresh-table" class="table table-bordered table-responsive-lg">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Logo</th>
                <th scope="col">Website</th>
                <th scope="col">Feed_xml</th>
                <th scope="col">Active</th>
                <th scope="col" data-field="active" data-sortable="true">Actions</th>
            </tr>
            </thead>
            <tbody>


            @if($merchants)

                @foreach($merchants as $merchant)

                    <tr>
                        <td>{{$merchant->id}}</td>
                        <td>
                            <a href="{{route('items',$merchant->id)}}">
                            {{$merchant->name}}
                            </a></td>
                        <td>{{$merchant->description}}</td>
                        <td><img class="image-logo" src="{{$merchant->logo}}" alt=""></td>
                        <td><a href="{{$merchant->website}}">{{$merchant->website}}</a></td>
                        <td>{{$merchant->feed_xml}}</td>
                        <td>
                            @if ($merchant->active === 1)
                                Active
                            @endif
                            @if($merchant->active === 0)
                                Inactive
                            @endif
                        </td>
                        <td>
                            <a href="{{ route ('weflycheap.merchants.edit',$merchant->id) }}"
                               data-target="#deleteUserConfirmation"
                               class="btn btn-success">Bewerken</a>

                            <button class="deleteProduct btn btn-danger" data-id="{{$merchant->id}}"
                                    data-token="{{ csrf_token() }}">Verwijderen
                            </button>

                        </td>
                    </tr>
                @endforeach
            @endif()
            </tbody>
        </table>
    </div>
@stop


@section('scripts')
    @parent
    <script>
        $(".deleteProduct").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token]').attr('content')
                }
            });
            var id = $(this).data("id");
            $.ajax(
                {
                    url: "{!! url('smartbeheer/merchants/delete') !!}" + "/" + id,
                    type: 'DELETE',
                    dataType: "JSON",
                    data: {
                        '_token': $('meta[name=csrf-token]').attr("content"),
                    },
                    success: function (response) {
                        confirm('Are you sure?');
                    },
                    error: function (xhr) {
                    }
                });
            location.reload();
        });
    </script>
@stop

<style>
    .image-logo {
        width: 50px;
    }

    .image-logo:hover {
        width: 150px;
        opacity: 1;
        transform: scale(1.0, 1.5);
    }
</style>
